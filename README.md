Team Tasks
An issue tracker for team activities unrelated to product development.
We are compiling a list of useful training resources.

## Generating a planning issue

1. Close out the previous milestone's planning issue.
1. Navigate to [this project's Pipelines page](https://gitlab.com/gitlab-com/create-stage/remote-development/-/pipelines).
1. Run a new pipeline and the a new issue will be created from the template

## Updating the planning issue template

The planning issue template can be edited here: `.gitlab/issue_templates/planning_issue.md`